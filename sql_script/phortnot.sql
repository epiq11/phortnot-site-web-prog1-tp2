-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  jeu. 18 juin 2020 à 21:38
-- Version du serveur :  10.4.10-MariaDB
-- Version de PHP :  7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `phortnot`
--

-- --------------------------------------------------------

--
-- Structure de la table `blog`
--

DROP TABLE IF EXISTS `blog`;
CREATE TABLE IF NOT EXISTS `blog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titre` varchar(50) DEFAULT NULL,
  `article` text DEFAULT NULL,
  `date_creation` datetime DEFAULT current_timestamp(),
  `id_utilisateur` int(11) NOT NULL,
  PRIMARY KEY (`id`,`id_utilisateur`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `blog`
--

INSERT INTO `blog` (`id`, `titre`, `article`, `date_creation`, `id_utilisateur`) VALUES
(3, 'Test blog', ' Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi massa ex, vestibulum non congue eu, posuere ac libero. Aenean et suscipit orci, vel commodo lectus. Aenean mollis volutpat diam. Morbi a sodales augue, sed rutrum ipsum. Nullam sit amet lectus molestie, placerat quam quis, dapibus mi. Nullam laoreet vulputate orci, sit amet posuere elit dignissim vel. Fusce eu euismod risus. Ut lacus nulla, dignissim ut congue nec, consequat eu justo. Donec non felis eu mi congue consequat nec et mauris. Cras aliquet tortor sed ipsum egestas hendrerit. Curabitur mollis dictum turpis vel ultrices. Nullam eu lectus urna. Cras auctor, enim vel porttitor condimentum, neque massa fringilla odio, quis vehicula turpis ligula non risus. Sed fringilla tortor eget sapien dictum commodo. Integer non condimentum sapien.\r\n\r\nDonec lectus mauris, pretium sit amet convallis sit amet, pellentesque a dolor. Quisque venenatis leo lectus, sit amet egestas enim consequat eu. Aliquam nisl turpis, sollicitudin vitae libero eu, luctus tincidunt nulla. Aliquam feugiat eget odio in maximus. Cras accumsan quam velit, id bibendum turpis tristique et. Sed eget nisl a leo convallis pellentesque. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas ullamcorper sed justo non luctus. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Curabitur scelerisque neque vitae tortor fermentum elementum. Mauris lectus felis, sodales ac viverra ac, rutrum et diam. Nunc in eleifend tortor. ', '2020-06-18 08:40:03', 7),
(4, 'Allo Prof123', 'Les textes littÃ©raires visent avant tout Ã  stimuler lâ€™imaginaire du lecteur. Ils peuvent Ã©galement transmettre de lâ€™information ou susciter la rÃ©flexion, voire le dÃ©bat dâ€™idÃ©es, mais leur principale caractÃ©ristiqueâ€‹ rÃ©side dans le travail que lâ€™auteur a effectuÃ© sur le style et la forme. Les textes littÃ©raires sont des Å“uvres que lâ€™on dit artistiques puisque les auteurs littÃ©raires ont des prÃ©occupations esthÃ©tiques afin de capter lâ€™intÃ©rÃªt du lecteur. Ils choisissent les mots appropriÃ©s pour exprimer leurs idÃ©es soigneusement tout en respectant un certain style.â€‹', '2020-06-18 08:51:30', 7),
(5, 'ThÃ©orie du texte', 'Le texte ne doit pas Ãªtre confondu avec l\'Å“uvre. Une Å“uvre est un objet fini, computable, qui peut occuper un espace physique (prendre place par exemple sur les rayons d\'une bibliothÃ¨que) ; le texte est un champ mÃ©thodologique ; on ne peut donc dÃ©nombrer (du moins rÃ©guliÃ¨rement) des textes ; tout ce qu\'on peut dire, c\'est que, dans telle ou telle Å“uvre, il y a (ou il n\'y a pas) du texte : Â« L\'Å“uvre se tient dans la main, le texte dans le langage. Â» On peut dire d\'une autre faÃ§on que, si l\'Å“uvre peut Ãªtre dÃ©finie en termes hÃ©tÃ©rogÃ¨nes au langage (allant du format du livre aux dÃ©terminations socio-historiques qui ont produit ce livre), le texte, lui, reste de part en part homogÃ¨ne au langage : il n\'est que langage et ne peut exister qu\'Ã  travers un autre langage. Autrement dit, Â« le texte ne s\'Ã©prouve que dans un travail, une production Â» : par la signifiance.', '2020-06-18 08:52:04', 7),
(6, 'nouoveau article', 'je fais un article.', '2020-06-18 11:43:32', 7);

-- --------------------------------------------------------

--
-- Structure de la table `menu`
--

DROP TABLE IF EXISTS `menu`;
CREATE TABLE IF NOT EXISTS `menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(45) DEFAULT NULL,
  `chemin` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `menu`
--

INSERT INTO `menu` (`id`, `nom`, `chemin`) VALUES
(1, 'Acceuil', '/'),
(2, 'Champions', 'champions.php'),
(3, 'Blog', 'blog.php'),
(4, 'Profil', 'profil.php'),
(5, 'Inscription/login', 'inscription.php');

-- --------------------------------------------------------

--
-- Structure de la table `utilisateur`
--

DROP TABLE IF EXISTS `utilisateur`;
CREATE TABLE IF NOT EXISTS `utilisateur` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(45) DEFAULT NULL,
  `prenom` varchar(45) DEFAULT NULL,
  `mot_de_passe` varchar(255) DEFAULT '',
  `telephone` varchar(12) DEFAULT NULL,
  `courriel` varchar(255) DEFAULT NULL,
  `date_naissance` date DEFAULT NULL,
  `nombre_de_participation` int(11) DEFAULT NULL,
  `date_deces` datetime DEFAULT NULL,
  `est_admin` smallint(6) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `utilisateur`
--

INSERT INTO `utilisateur` (`id`, `nom`, `prenom`, `mot_de_passe`, `telephone`, `courriel`, `date_naissance`, `nombre_de_participation`, `date_deces`, `est_admin`) VALUES
(7, 'Bergeron', 'eric', '$2y$10$HoGrmhkCwA99pZZyPDXWQugMTmXLIEsAtL2B6ahpGeWnvv7rtzhPu', '412-365-4789', 'epiq11@gmail.com', '1997-09-17', 100, '2019-06-01 00:00:00', 1),
(8, 'Berger', 'lec', '$2y$10$F1LBTv2Aks66oLBDL2Eigu5K.ukTUvxarM/thAvgwyCopoQKRQnee', '987-456-1234', 'lec@email.com', '1999-07-14', 200, '2020-02-09 00:00:00', 1),
(9, 'Berger', 'luci', '$2y$10$tendi9z0COV0U1nxksniXuTP7z6wtS9gSXyaW012/XqstqnHJuu/C', '456-123-7897', 'luci@email.com', '1998-05-05', NULL, NULL, 0);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
