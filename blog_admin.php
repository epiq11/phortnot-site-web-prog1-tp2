<?php 
session_start();

include_once './functions/data/connecteur.php';
include_once './functions/data/champion.php';
include_once './functions/data/profil.php';
include_once './functions/data/blog.php';
include_once './includes/parts/header.php';
include_once './functions/form-functions.php';

////////////////////////////////////////////////
//////////////////////////////////////

$prenom = (isset($_SESSION["prenom"]) ? $_SESSION["prenom"] : "");
$session = (isset($_SESSION["est_admin"]) ? $_SESSION["est_admin"] : "");
$query = selection_profil($prenom)->fetch();


$errors_titre = (isset($errors["titre"]) ? $errors["titre"] : "");
$errors_texte =  (isset($errors["texte"]) ? $errors["texte"] : "");

// $user_update_return = user_update();

?>
  <main id="page">

<?php

 if ($session == 1){

  $return_supprimer_article = null;
  $resultat_ajout_article = null;
  $result_blog_update = null;
  $titre = "";
  $texte = "";

  if ($_SERVER["REQUEST_METHOD"] == "POST") {

    if(!empty($_SESSION["id_blog"])){

      $result_blog_update = blog_update();
      $_SESSION["id_blog"] = null;
      // header("Refresh:0");
    }else{
      $resultat_ajout_article = nouveau_article();
    } 
  }

  if ($_SERVER["REQUEST_METHOD"] == "GET") {

    $get_id = (isset($_GET["id"]) ? $_GET["id"] : "");
    $get_supprimer = (isset($_GET["supprimer"]) ? $_GET["supprimer"] : "");

    if( !empty($get_id) ){

        $query_afficher = blog_afficher($get_id)->fetch();
        $titre = $query_afficher["titre"];
        $texte = $query_afficher["article"];
        $_SESSION["id_blog"] = $query_afficher["id"];
    }

    if (!empty($get_supprimer)) {
      $return_supprimer_article = supprimer_article();
    }
  }

  
  
  ?>
    <h1>Administration du blog</h1>
    <section class="container justifyCenter">
      <div class="contenu">
        <h2>Création/modification d'un article</h2> 

        <form action="/blog_admin.php" method="POST" id="formulaireblog" name="formulaireblog">
          <fieldset>
          <?php echo $resultat_ajout_article ?>
          <?php echo $result_blog_update ?>

            <label for="titre">Titre :</label>
            <input type="text" name="titre" id="titre" placeholder="Titre de l'article" value="<?php echo $titre; ?>">
            <span class="span"><?php echo $errors_titre ?></span> <br><br>

            <label for="texte">Texte :</label>
            <textarea name="texte" id="texte" cols="30" rows="10"><?php echo $texte; ?></textarea>
            <span class="span"><?php echo $errors_texte ?></span>
          </fieldset>
          <input id="button" class="button" type="submit" name="boutonNouveauArticle" value="Soumettre">
        </form>
      </div>

      <div class="contenu">
        <h2>Liste des articles.</h2> 
        <?php echo $return_supprimer_article ?>
        <table>
          <thead>
            <tr>
              <th>
                Modifier/ supprimer
              </th>
              <th>
                Créateur
              </th>
              <th>
                Date création
              </th>
              <th>
                Titre
              </th>
              <th>
                Texte
              </th>
            </tr>
          </thead>
          <tbody>

            <?php

              $query = liste_article()->fetchAll();

              foreach ($query as $row) {
              $id = $row["id"];
              $id_createur = $row["id_utilisateur"];
              $date_creation = $row["date_creation"];
              $titre = $row["titre"];
              $texte = $row["article"];
              $utilisateur_nom = $row["utilisateur_nom"];
              $utilisateur_prenom = $row["utilisateur_prenom"];

                ?>
                <tr>
                  <td>
                    <a href="/blog_admin.php?id=<?php echo $id; ?>">Modifier</a><br/>
                    <a href="/blog_admin.php?supprimer=<?php echo $id; ?>">Supprimer</a>
                  </td>
                  <td>
                    <?php echo $utilisateur_prenom . " " . $utilisateur_nom; ?>
                  </td>
                  <td>
                    <?php echo date('d-m-Y H:i:s', strtotime($date_creation)); ?>
                  </td>
                  <td>
                    <?php echo validate_text_fields($titre); ?>
                  </td>
                  <td>
                    <?php echo substr(validate_text_fields($texte), 0, 30) . "..."; ?>
                  </td>
                </tr>
                
               <?php
              }
            ?>

          </tbody>

        </table>
      </div>
    </section>
  <?php

}
else{
?>

    <section class="container justifyCenter">
      <div class="contenu"> 
        <p>Vous n'avez pas accès à l'administration du blog.</p>
      </div>
    </section>

<?php
}
?>
  </main>
<?php
  include_once './includes/parts/footer.php';
?>