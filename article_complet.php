<?php 
include_once './functions/data/connecteur.php';
include_once './functions/data/blog.php';
include_once './includes/parts/header.php';

$get_id = null;

if ($_SERVER["REQUEST_METHOD"] == "GET") {

  $get_id = (isset($_GET["id"]) ? $_GET["id"] : "");
}

?>

  <main id="page">
    <section class="container container_blog justifyCenter">
      <div class="contenu">
        <h1>Articles du Blog</h1>

      </div>

      <?php
        $query = recheche_un_article($get_id)->fetchAll();
        
        foreach ($query as $row) {
          $id = $row["id"];
          $date_creation = $row["date_creation"];
          $titre = $row["titre"];
          $texte = $row["article"];
          $utilisateur_nom = $row["utilisateur_nom"];
          $utilisateur_prenom = $row["utilisateur_prenom"];
      ?>

          <article class="contenu contenuBackground">
            <header>
              <h2><?php echo validate_text_fields($titre); ?></h2>
            </header>
            <main>
              <p><?php echo validate_text_fields($texte); ?></p>
            </main>
            <footer>
              <p>Créer par <strong><?php echo validate_text_fields($utilisateur_prenom) . " " . validate_text_fields($utilisateur_nom) . "</strong> le " . date('d-m-Y H:i:s', strtotime($date_creation)) ; ?></p>
            </footer>

          </article>

      <?php
        }
      ?>




    </section>

  </main>

<?php 
include_once './includes/parts/footer.php';
?>