<?php 
include_once './functions/data/connecteur.php';
include_once './functions/data/blog.php';
include_once './includes/parts/header.php';

?>

  <main id="page">
    <section class="container container_blog justifyCenter">
      <div class="contenu">
        <h1>Articles du Blog</h1>

      </div>

      <?php
        $query = liste_article()->fetchAll();
        
        foreach ($query as $row) {
          $id = $row["id"];
          $date_creation = $row["date_creation"];
          $titre = $row["titre"];
          $texte = $row["article"];
          $utilisateur_nom = $row["utilisateur_nom"];
          $utilisateur_prenom = $row["utilisateur_prenom"];
      ?>

          <article class="contenu contenuBackground">
            <header>
              <h2><?php echo validate_text_fields($titre); ?></h2>
            </header>
            <main>
              <p><?php echo substr(validate_text_fields($texte), 0, 200) . "..."; ?></p>

              <form action="/article_complet.php" method="GET" >
                <input type="hidden" name="id" value="<?php echo validate_text_fields($id); ?>">
                <input type="submit" class="button" value="Voir suite...">
              </form>

            </main>
            <footer>
              <p>Créer par <strong><?php echo validate_text_fields($utilisateur_prenom) . " " . validate_text_fields($utilisateur_nom) . "</strong> le " . date('d-m-Y H:i:s', strtotime($date_creation)) ; ?></p>
            </footer>

          </article>

      <?php
        }
      ?>




    </section>

  </main>

<?php 
include_once './includes/parts/footer.php';
?>