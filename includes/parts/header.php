<?php
  include_once './functions/data/connecteur.php';
  include_once '././functions/data/menu.php';
  include_once './functions/form-functions.php';

?>

<!doctype html>
<html class="no-js" lang="">

<head>
  <meta charset="utf-8">
  <title>Phortnot</title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link rel="manifest" href="site.webmanifest">
  <link rel="apple-touch-icon" href="icon.png">
  <!-- Place favicon.ico in the root directory -->

  <link rel="stylesheet" href="css/normalize.css">
  <link rel="stylesheet" href="css/main.css">
  <link rel="stylesheet" href="css/styles.css">
  <link rel="stylesheet" href="css/publicite_svg.css">

  <meta name="theme-color" content="#fafafa">
</head>

<body>


  <header id="header">
    <div class="container">
      <div id="logo">
        <h1 ><a href="/">PHORTNOT</a></h1>
      </div>
      <nav id="menu" class="show-on-mobile">
        <ul>
          <li><button type="button" id="activate-menu">Menu</button></li>
        </ul>
      </nav>


      <nav id="menuPrincipale" class="hide-on-mobile">
        <ul>
          <?php

          $query = menu()->fetchAll();

          foreach ($query as $cle) {
          ?>

            <li><a href="<?php echo validate_text_fields($cle["chemin"]); ?>"><?php echo validate_text_fields($cle["nom"]); ?></a></li>
    
          <?php
          }
          ?>

        </ul>
      </nav>
    </div>
  </header>