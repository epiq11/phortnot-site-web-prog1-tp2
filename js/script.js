

// let anime_img_champion = anime({
//     targets: '.img_champion',
//     duration: 1000,
//     keyframes: [
//         { scale: 0.01 },
//         { scale: 100 }
//     ],
// });
$(document).ready(function () {
    $(".img_champion").animate({
        width: "30%"
    }, 1000, function () {
        $(".img_champion").after('<div class="text_on_img_champion"><h2>CHAMPION!</h2></div>');
        $(".text_on_img_champion").css({ "zindex": "1", "color": "white", "position": "absolute", "top": "25%", "left": "36%", "rotate": "-30deg" });
        $(".text_on_img_champion h2").css({ "font-size": "5vw", "opacity": "0.8" });
    })
})



let animePub = anime({
    targets: '.pub',
    duration: 4000,
    keyframes: [
        { opacity: 0.1 },
        { opacity: 0.2 },
        { opacity: 0.3 },
        { opacity: 0.4 },
        { opacity: 0.5 },
        { opacity: 0.6 },
        { opacity: 0.7 },
        { opacity: 0.8 },
        { opacity: 0.9 },
        { opacity: 1 }
    ],
    complete: function () {
        $("#kf1-lignes, #kf1-phortnot, #kf2_texte, #bonhomme, #cible, #fond-couleur, #Ellipse, #texte-ouvert, #texte-tournoi").css("animation-play-state", "running");
    }
});


let animLogo = anime.timeline({
    targets: '#logo',
})

animLogo
    .add({
        translateY: {
            value: ['-60px', '0px'],
            duration: 575,
            easing: 'easeInQuad',
        }
    })
    .add({
        translateY: {
            value: ['0px', '-25px'],
            duration: 575,
            easing: 'easeOutQuad',
        }
    })
    .add({
        translateY: {
            value: ['-25px', '0px'],
            duration: 575,
            easing: 'easeInQuad',
        }
    })
    .add({
        translateY: {
            value: ['0px', '-15px'],
            duration: 575,
            easing: 'easeOutQuad',
        }
    })
    .add({
        translateY: {
            value: ['-15px', '0px'],
            duration: 575,
            easing: 'easeInQuad',
        }
    })

// animation du menu
$("nav li").hover(function () {
    $(this).animate({ letterSpacing: "5px" }, "slow");
}, function () {
    $(this).animate({ letterSpacing: "0px" }, "slow");
});

// Menu mobile
var activateBtn = document.getElementById("activate-menu");

activateBtn.addEventListener("click", function () {
    var activateElement = document.getElementById("menuPrincipale");
    activateElement.classList.toggle("hide-on-mobile");
})