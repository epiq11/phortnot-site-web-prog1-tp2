<?php 
include_once './functions/data/connecteur.php';
include_once './functions/data/champion.php';
include_once './includes/parts/header.php';
include_once './functions/form-functions.php';


?>

  <main id="page">
    <section class="container justifyCenter">
      <div class="contenu_champions">
        <h1>Champions</h1>
        <table>
          <thead>
            <tr>
              <th>
                Prénom
              </th>
              <th>
                Nom
              </th>
              <th>
                Date/heure du décès
              </th>
              <th>
                Détails
              </th>
            </tr>
          </thead>
          <tbody>

            <?php

              $query = liste_champions()->fetchAll();
              
              foreach ($query as $row) {
              $date_deces = $row["date_deces"];
              $id = $row["id"];
                ?>
                <tr>
                  <td>
                    <?php echo validate_text_fields($row["prenom"]); ?>
                  </td>
                  <td>
                    <?php echo validate_text_fields($row["nom"]); ?>
                  </td>
                  <td>
                    <?php echo date('d-m-Y H:i:s', strtotime($date_deces)); ?>
                  </td>
                  <td>
                    <a href="/statistiques_champion.php?id_champion=<?php echo validate_text_fields($id); ?>">détails</a>
                  </td>
                </tr>
                
               <?php
              }
            ?>

          </tbody>

        </table>

      </div>

    </section>


  </main>

<?php 
include_once './includes/parts/footer.php';
?>