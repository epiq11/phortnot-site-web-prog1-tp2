<?php 
session_start();

include_once './functions/data/connecteur.php';
include_once './functions/data/champion.php';
include_once './functions/data/profil.php';
include_once './includes/parts/header.php';
include_once './functions/form-functions.php';

////////////////////////////////////////////////
//////////////////////////////////////

$prenom = (isset($_SESSION["prenom"]) ? $_SESSION["prenom"] : "");

$errors_prenomConnexion = (isset($errors["prenomConnexion"]) ? $errors["prenomConnexion"] : "");
$errors_prenom =  (isset($errors["prenom"]) ? $errors["prenom"] : "");
$errors_nom =  (isset($errors["nom"]) ? $errors["nom"] : "");
$errors_mot_de_passe =  (isset($errors["mot_de_passe"]) ? $errors["mot_de_passe"] : "");
$errors_telephone =  (isset($errors["telephone"]) ? $errors["telephone"] : "");
$errors_dateNaissance =  (isset($errors["dateNaissance"]) ? $errors["dateNaissance"] : "");
$errors_courriel =  (isset($errors["courriel"]) ? $errors["courriel"] : "");


$user_update_return = user_update();

$query = selection_profil($prenom)->fetch();

?>
  <main id="page">

<?php
      if (isset($_POST["boutonDeconexion"])){
          session_destroy();
          header("Refresh:0");
      }


 if (isset($_SESSION['prenom']) && isset($query["prenom"])){

  $_SESSION["id"] = (isset($_SESSION["id"]) ? $query["id"] : "");
  $affiche_status_utilisateur = ($_SESSION["est_admin"] == 1 ? " (Admin)" : " (Utilisateur)");

    $nom = validate_text_fields($query["nom"]);
    $prenom = validate_text_fields($query["prenom"]);
    $telephone = $query["telephone"];
    $courriel = validate_text_fields($query["courriel"]);
    $date_naissance = $query["date_naissance"];

  ?>
    <section class="container justifyCenter">
      <div class="contenu">
        <h1>Votre profil: <?php echo $prenom . $affiche_status_utilisateur; ?></h1> 
        <a href="blog_admin.php">ADMINISTRATION DU BLOG</a>
        <form action="/profil.php" method="POST" id="formulaireDeconexion" name="formulaireDeconexion">
          <input id="button" class="button" type="submit" name="boutonDeconexion" value="Déconnexion">
        </form>

        <form action="/profil.php" method="POST" id="formulaireProfil" name="formulaireProfil">
          <fieldset>
            <?php echo $user_update_return ?>
            <label for="prenom">Prénom :</label>
            <input type="text" name="prenom" placeholder="Prénom" value="<?php echo $prenom; ?>">
            <span class="span"><?php echo $errors_prenom ?></span> <br><br>

            <label for="nom">Nom :</label>
            <input type="text" name="nom" placeholder="Nom" value="<?php echo $nom; ?>">
            <span class="span"><?php echo $errors_nom ?></span> <br><br>

            <label for="mot_de_passe">Mot de passe :</label>
            <input type="password" name="mot_de_passe" value="<?php echo ""; ?>">
            <span class="span"><?php echo $errors_mot_de_passe ?></span> <br><br>

            <label for="telephone">Téléphone :</label>
            <input type="text" name="telephone" placeholder="123-456-7890" value="<?php echo $telephone; ?>">
            <span class="span"><?php echo $errors_telephone ?></span> <br><br>

            <label for="dateNaissance">Date de naissance :</label>
            <input type="date" id="dateNaissance" name="dateNaissance" value="<?php echo $date_naissance; ?>">
            <span class="span"><?php echo $errors_dateNaissance ?></span> <br><br>

            <label for="courriel">Courriel :</label>
            <input type="text" name="courriel" placeholder="xxx@xxxx.xxx" value="<?php echo $courriel; ?>">
            <span class="span"><?php echo $errors_courriel ?></span> <br><br>
          </fieldset>
          <input id="button" class="button" type="submit" name="boutonProfil" value="Soumettre">
        </form>
      </div>
    </section>

  <?php

}
else{
?>

    <section class="container justifyCenter">
      <div class="contenu"> 
        <p>Veillez-vous connecter, <a href="inscription.php">cliquez-ici</a>.</p>
      </div>
    </section>

<?php
}
?>
  </main>
<?php
  include_once './includes/parts/footer.php';
?>