<?php
$errors = null;

function form_values($nom){
    echo (isset($_POST[$nom]) ? $_POST[$nom] : "");
}

function validate_text_fields($text){
    return trim(htmlspecialchars($text));
}

function validate_form(){
        $errors = [];
        


        if (isset($_POST["boutonNouveauArticle"])){
            $errors["titre"] = (empty($_POST["titre"])) ? "Ajouter un Titre." : "";
            $errors["texte"] = (empty($_POST["texte"])) ? "Ajouter du texte." : "";
        }
        
        if (isset($_POST["boutonConnexion"])){
            $errors["prenomConnexion"] = (empty($_POST["prenomConnexion"])) ? "Inscrire un prénom." : "";
            $errors["mot_de_passeConnexion"] = (empty($_POST["mot_de_passeConnexion"])) ? "Inscrire un mot_de_passe." : "";
        }

        if (isset($_POST["boutonInscription"])){


            $errors["prenom"] = (empty($_POST["prenom"])) ? "Inscrire un prénom." : "";
            $errors["nom"] = (empty($_POST["nom"])) ? "Le nom ne peut pas être vide." : "";
            $errors["mot_de_passe"] = (empty($_POST["mot_de_passe"])) ? "Le mot de passe ne peut pas être vide." : "";
            $errors["telephone"] = (empty(validate_tel($_POST["telephone"]))) ? "Le téléphone ne peut pas être vide et il doit être formater correctement." : "";
            $errors["dateNaissance"] = (empty($_POST["dateNaissance"])) ? "La date de naissance ne peut pas être vide." : "";
            $errors["courriel"] = (empty(validate_email($_POST["courriel"]))) ? "Le courriel ne peut pas être vide et il doit être formatez correctement." : "";
        }

        if (isset($_POST["boutonProfil"])){
            $errors["prenom"] = (empty($_POST["prenom"])) ? "Inscrire un prénom." : "";
            $errors["nom"] = (empty($_POST["nom"])) ? "Le nom ne peut pas être vide." : "";
            $errors["telephone"] = (empty(validate_tel($_POST["telephone"]))) ? "Le téléphone ne peut pas être vide et il doit être formater correctement." : "";
            $errors["dateNaissance"] = (empty($_POST["dateNaissance"])) ? "La date de naissance ne peut pas être vide." : "";
            $errors["courriel"] = (empty(validate_email($_POST["courriel"]))) ? "Le courriel ne peut pas être vide et il doit être formatez correctement." : "";
        }


        if (count($errors) >= 1 ) {
            return $errors;
        }
}

function validate_email($courriel){
    return filter_var($courriel, FILTER_VALIDATE_EMAIL);
}

function validate_tel($tel){
    return filter_var($tel, FILTER_VALIDATE_REGEXP, array("options"=>array("regexp"=>'/[0-9]{3}[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4}/')));
}

function new_inscription(){
    
    $nom = (isset($_POST["nom"]) ? $_POST["nom"] : "");
    $prenom = (isset($_POST["prenom"]) ? $_POST["prenom"] : "");
    $mot_de_passe = (isset($_POST["mot_de_passe"]) ? password_hash($_POST["mot_de_passe"], PASSWORD_DEFAULT) : "");
    $telephone = (isset($_POST["telephone"]) ? $_POST["telephone"] : "");
    $courriel = (isset($_POST["courriel"]) ? $_POST["courriel"] : "");
    $date_naissance = (isset($_POST["dateNaissance"]) ? $_POST["dateNaissance"] : null);
    $nombre_de_participation = (isset($_POST["nombre_de_participation"]) ? $_POST["nombre_de_participation"] : "");
    $errors = validate_form();
    $errors = (isset($errors)) ? validate_form() : "";
    $errors_count = -1;

    if (is_array($errors)){
        $errors_count = count($errors);
        foreach ($errors as $key => $value){
            if (empty($value)){
                $errors_count = $errors_count - 1;
            }
        }
    }

    if($errors_count == 0){

        $conn = connect();

        if (isset($_POST["boutonInscription"]) && !empty($date_naissance)) {

            try{
                $pdo = $conn->prepare("INSERT INTO utilisateur (nom, prenom, mot_de_passe, telephone, courriel, date_naissance ) 
                                    VALUES (:nom,:prenom,:mot_de_passe,:telephone,:courriel,:date_naissance)"); 

                $pdo->bindParam(':nom',$nom);
                $pdo->bindParam(':prenom',$prenom);
                $pdo->bindParam(':mot_de_passe',$mot_de_passe);
                $pdo->bindParam(':telephone',$telephone);
                $pdo->bindParam(':courriel',$courriel);
                $pdo->bindParam(':date_naissance',$date_naissance);

                $pdo->execute([$nom,$prenom,$mot_de_passe,$telephone,$courriel,$date_naissance]); 

                return  '<span class="span textGreen justifyCenter">Inscription ajoutée dans la table.</span> <br/>';
            }catch(PDOException $e){
                return "Erreur : " . $e->getMessage();
                $conn->rollBack();
            }
            $conn = null;
        }
    }
}

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $form = validate_form();

    if(is_array($form)){
        if (count($form) > 0 ) {
            $errors = $form;
        }
    }

    return $errors;
}

?>