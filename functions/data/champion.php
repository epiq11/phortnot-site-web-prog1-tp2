<?php

function liste_champions(){
    $conn = connect();
    try{
        $requete = $conn->query("SELECT * FROM utilisateur 
                                    Where date_deces IS NOT NULL 
                                    ORDER BY date_deces DESC");
        return $requete;
    } catch(PDOException $e){
        echo 'Erreur : ' . $e->getMessage();
    }
    $conn = null;
    Exit();
}

function selection_champions($id){
    $conn = connect();
    try{
        $requete = $conn->prepare("SELECT * FROM utilisateur Where id = :id");
        $requete->execute([":id"=>$id]);
        return $requete;
    } catch(PDOException $e){
        echo 'Erreur : ' . $e->getMessage();
    }
    $conn = null;
    Exit();
}