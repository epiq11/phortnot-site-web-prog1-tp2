<?php

function selection_profil($prenom = ""){
    $conn = connect();
    try{
        $requete = $conn->prepare("SELECT * FROM utilisateur Where prenom = :prenom");
        $requete->execute([":prenom"=>$prenom]);
        return $requete;
    } catch(PDOException $e){
        echo 'Erreur : ' . $e->getMessage();
    }
    $conn = null;
    Exit();
}

function verif_mot_de_passe($mdp = "", $prenom ){
    $conn = connect();
    $resultat = null;
    $result_select_profil = selection_profil($prenom)->fetch();


    if (password_verify($mdp, $result_select_profil["mot_de_passe"])){
         $resultat[0] = 1;
         $resultat[1] = $result_select_profil["prenom"];
         $resultat[2] = $result_select_profil["id"];
         $resultat[3] = $result_select_profil["est_admin"];
    }
    else{
         $resultat = 0;
    }

return $resultat;

}

function login($mot_de_passe, $prenom){

    $resultat = null;
    $infoConnecter = "";
    $resultat = verif_mot_de_passe($mot_de_passe, $prenom );

if ($_SERVER["REQUEST_METHOD"] == "POST")
{

    if ( $resultat[0] == 1){
        $_SESSION['prenom'] = $resultat[1];
        $_SESSION['id'] = $resultat[2];
        $_SESSION['est_admin'] = $resultat[3];
        $infoConnecter = '<span class="span textGreen">Connecter en tant qu\''. $_SESSION['prenom'] . '</span>';
    }
    else
    {
        $infoConnecter = '<span class="span">Information de connection incorrecte.</span>';
    }

    return $infoConnecter;
}
            
}

function user_update(){
    
    
    $nom = (isset($_POST["nom"]) ? $_POST["nom"] : "");
    $prenom = (isset($_POST["prenom"]) ? $_POST["prenom"] : "");
    $mot_de_passe = (!empty($_POST["mot_de_passe"]) ? password_hash($_POST["mot_de_passe"], PASSWORD_DEFAULT) : "");
    $telephone = (isset($_POST["telephone"]) ? $_POST["telephone"] : "");
    $courriel = (isset($_POST["courriel"]) ? $_POST["courriel"] : "");
    $date_naissance = (isset($_POST["dateNaissance"]) ? $_POST["dateNaissance"] : null);
    $nombre_de_participation = (isset($_POST["nombre_de_participation"]) ? $_POST["nombre_de_participation"] : "");
    

    $errors = validate_form();
    $errors = (isset($errors)) ? validate_form() : "";
    $errors_count = -1;

    if (is_array($errors)){
        $errors_count = count($errors);
        foreach ($errors as $key => $value){
            if (empty($value)){
                $errors_count = $errors_count - 1;
            }
        }
    }

    if($errors_count == 0){ 

        $conn = connect();

        if (isset($_POST["boutonProfil"]) && !empty($date_naissance)) {

            try{
                $pdo = $conn->prepare("UPDATE utilisateur SET nom = :nom, prenom = :prenom, telephone = :telephone, courriel = :courriel, date_naissance = :date_naissance WHERE id = :id"); 
              
                $pdo->bindParam(':nom',$nom);
                $pdo->bindParam(':prenom',$prenom);

                // if (!empty($_POST["mot_de_passe"])) {
                //     $pdo->bindParam(':mot_de_passe',$mot_de_passe);
                // }

                $pdo->bindParam(':telephone',$telephone);
                $pdo->bindParam(':courriel',$courriel);
                $pdo->bindParam(':date_naissance',$date_naissance);
                $pdo->bindParam(':id',$_SESSION["id"]);

                $pdo->execute(); 
                return  '<span class="span textGreen
                 justifyCenter">Modification inscrit dans la base de donnée.</span> <br/>';
            }catch(PDOException $e){
                return "Erreur : " . $e->getMessage();
                $conn->rollBack();
            }
            $conn = null;
        }
    }
}

