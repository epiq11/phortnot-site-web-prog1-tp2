<?php

function menu(){
    $conn = connect();
    try{
        $requete = $conn->query("SELECT * FROM menu");
        return $requete;
    } catch(PDOException $e){
        echo 'Erreur : ' . $e->getMessage();
    }
    $conn = null;
    Exit();
}
