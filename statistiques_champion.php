<?php 
include_once './functions/data/connecteur.php';
include_once './functions/data/champion.php';
include_once './includes/parts/header.php';
include_once './functions/form-functions.php';

$id_champions_a_afficher = $_GET["id_champion"];

$query = selection_champions($id_champions_a_afficher)->fetchAll();

foreach ($query as $cle) {
  $nom = $cle["nom"];
  $prenom = $cle["prenom"];
  $nombre_de_participation = $cle["nombre_de_participation"];
  $date_deces = $cle["date_deces"];
}
   
?>

  <main id="page">
    <section class="container justifyCenter">
      <div class="contenu">
        <h1>Champion <?php echo $prenom . " " . $nom; ?> </h1>
        <div class="div_img_champion">
          <img class="img_champion" src="/img/avatar.png" alt="Avatar Champion">
        </div>
        <div>Nombre de participation : <?php echo validate_text_fields($nombre_de_participation); ?></div>
        <div>Date de décès : <?php echo validate_text_fields($date_deces); ?></div>
      </div>

    </section>


  </main>

<?php 
include_once './includes/parts/footer.php';
?>