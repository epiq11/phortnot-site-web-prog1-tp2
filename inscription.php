<?php 
session_start();

include_once './functions/data/connecteur.php';
include_once './includes/parts/header.php';
include_once './functions/form-functions.php';
include_once './functions/data/profil.php';

// $mot_de_passeConnexion = NULL;
// $prenom_connexion = NULL;



$mot_de_passeConnexion = (isset($_POST["mot_de_passeConnexion"]) ? $_POST["mot_de_passeConnexion"] : "");
$prenom_connexion = (isset($_POST["prenomConnexion"]) ? $_POST["prenomConnexion"] : "");

$infoConnecter = login($mot_de_passeConnexion, $prenom_connexion);

///////////////////////

$new_inscription_return = new_inscription();

$errors_prenomConnexion = (isset($errors["prenomConnexion"]) ? $errors["prenomConnexion"] : "");
$errors_mot_de_passeConnexion = (isset($errors["mot_de_passeConnexion"]) ? $errors["mot_de_passeConnexion"] : "");
$errors_prenom =  (isset($errors["prenom"]) ? $errors["prenom"] : "");
$errors_nom =  (isset($errors["nom"]) ? $errors["nom"] : "");
$errors_mot_de_passe =  (isset($errors["mot_de_passe"]) ? $errors["mot_de_passe"] : "");
$errors_telephone =  (isset($errors["telephone"]) ? $errors["telephone"] : "");
$errors_dateNaissance =  (isset($errors["dateNaissance"]) ? $errors["dateNaissance"] : "");
$errors_courriel =  (isset($errors["courriel"]) ? $errors["courriel"] : "");


?>

  <main id="page">
    <section class="container justifyCenter">
      <div class="contenu">
        <h1>Connexion</h1>
        <form action="/inscription.php" method="POST" id="formulaireConnexion" name="formulaireConnexion">
            <fieldset>

              <label for="prenomConnexion">Prénom :</label>
              <input type="text" name="prenomConnexion" placeholder="Prénom"><br>
              <?php

                if ($errors_prenomConnexion != ""){
                echo '<span class="span">'. $errors_prenomConnexion . '</span><br/>';
                
                }
                if($infoConnecter != "" ){
                echo $infoConnecter;

                }
              
              ?>

              <label for="mot_de_passeConnexion">Mot de passe :</label>
              <input type="password" name="mot_de_passeConnexion" placeholder="Mot de passe">
              <?php

                if ($errors_mot_de_passeConnexion != ""){
                echo '<span class="span">'. $errors_mot_de_passeConnexion . '</span>';
                
                }
                if($infoConnecter != "" ){
                echo $infoConnecter;

                }
              
              ?>

              
              
              
            </fieldset>

            <input id="button" class="button" type="submit" name="boutonConnexion" value="Soumettre">
          </form>
      </div>
      <hr/>
      
      <div class="contenu">
        <h1>Inscription</h1>
        <form action="/inscription.php" method="POST" id="formulaire" name="formulaire">
          <fieldset>
          <?php echo $new_inscription_return ?>
            <label for="prenom">Prénom :</label>
            <input type="text" name="prenom" placeholder="Prénom">
            <span class="span"><?php echo validate_text_fields($errors_prenom) ?></span> <br><br>

            <label for="nom">Nom :</label>
            <input type="text" name="nom" placeholder="Nom">
            <span class="span"><?php echo validate_text_fields($errors_nom) ?></span> <br><br>

            <label for="mot_de_passe">Mot de passe :</label>
            <input type="password" name="mot_de_passe" placeholder="Mot de passe">
            <span class="span"><?php echo validate_text_fields($errors_mot_de_passe) ?></span> <br><br>

            <label for="telephone">Téléphone :</label>
            <input type="text" name="telephone" placeholder="123-456-7890">
            <span class="span"><?php echo validate_text_fields($errors_telephone) ?></span> <br><br>

            <label for="dateNaissance">Date de naissance :</label>
            <input type="date" id="dateNaissance" name="dateNaissance">
            <span class="span"><?php echo validate_text_fields($errors_dateNaissance) ?></span> <br><br>

            <label for="courriel">Courriel :</label>
            <input type="text" name="courriel" placeholder="xxx@xxxx.xxx">
            <span class="span"><?php echo validate_text_fields($errors_courriel) ?></span> <br><br>

          </fieldset>

          <input id="button2" class="button2" type="submit" name="boutonInscription" value="Soumettre">
        </form>
      </div>

    </section>


  </main>

<?php 
include_once './includes/parts/footer.php';
?>